"""
LFortran
========

loop_unroll_factor | time (s)
load = 300000000
1 | 2.92
4 | 2.87
8 | 2.60
16 | 2.41
32 | 2.41
64 | 2.44
128 | 2.50
256 | 2.55
1024 | 4.24
gfortran = 1.87
lfortran(32 loop unroll)/gfortran = ~1.3

load = 600000000
1 | 5.76
4 | 5.72
8 | 5.15
16 | 4.79
32 | 4.79
64 | 4.85
128 | 4.97
256 | 5.05
1024 | 8.5
gfortran = 3.73
lfortran(32 loop unroll)/gfortran ~ 1.3

load = 1200000000
1 | 11.50
4 | 11.40
8 | 10.30
16 | 9.53
32 | 9.50
64 | 9.55
128 | 9.90
256 | 10.08
1024 | 16.88
gfortran = 7.45
lfortran(32 loop unroll)/gfortran ~ 1.3

load = 120000000
1 | 1.18
4 | 1.15
8 | 1.04
16 | 0.96
32 | 0.96
64 | 0.97
128 | 1
256 | 1.02
1024 | 1.70
gfortran = 0.75

Notes

With loop unroll factor of 32, LFortran shows best performance.
Without loop unrolling LFortran is 1.55 times slower than GFortran.
With loop unrolling (32 factor) LFortran is 1.3 times slower than GFortran.
This means loop unrolling alone is giving us ~20% gains.
"""
loop_unroll_factor = 2
load = 1200000000
variables = ["sin{}".format(i) for i in range(loop_unroll_factor)]
loop_line = "    sin{} = sin((i + {})*1.0_8)"

loop_body = []
for i in range(loop_unroll_factor):
    loop_body.append(loop_line.format(i, i))

lines = ["program loop_unroll",
         "implicit none",
         "integer(8) :: i",
         "integer(8) :: array_size = {}".format(load),
         "real(8) :: " + ', '.join(variables),
         "do i = 1_8, array_size, {}_8".format(loop_unroll_factor)]
lines.extend(loop_body)
lines.extend(["end do", "end program"])
print('\n'.join(lines))