import sys

rows, cols = int(sys.argv[1]), int(sys.argv[2])

lines = ["program mat_mat",
         "implicit none",
         "integer(4) :: i, j, k, rows, cols",
         "real(4), allocatable :: mat1(:,:), mat2(:, :), mat3(:, :)",
         "rows = {}".format(rows),
         "cols = {}".format(cols),
         "allocate(mat1(rows, cols), mat2(cols, rows), mat3(rows, rows))",
         "do i = 1, rows",
         "   do j = 1, cols",
         "       mat1(i, j) = (i + j) * 0.314",
         "   end do",
         "end do",
         "do i = 1, rows",
         "   do j = 1, cols",
         "       mat2(i, j) = (i * j) * 0.5",
         "   end do",
         "end do",
         "do i = 1, rows",
         "   do j = 1, rows",
         "       mat3(i, j) = 0.0",
         "       do k = 1, cols",
         "          mat3(i, j) = mat3(i, j) + mat1(i, k) * mat2(k, j)",
         "       end do",
         "   end do",
         "end do",
         "end program"]

print('\n'.join(lines))