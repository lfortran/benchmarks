Instructions for Running Benchmarks
-----------------------------------

**`mat_vec.sh`**

`bash mat_vec.sh <compiler_name> <min_runs_for_hyperfine> <warmup_for_hyper_fine> <compiler_options_optional> >> mat_vec_temp.out`

To commit your results, you may copy the output in `mat_vec_temp.out` to `mat_vec.out`. You can change the `kind` variable to 4 or 8 for single/double precision benchmarking.

**`mat_mat.sh`**

`bash mat_mat.sh <compiler_name> <min_runs_for_hyperfine> <warmup_for_hyper_fine> <compiler_options_optional> >> mat_mat_temp.out`

To commit your results, you may copy the output in `mat_mat_temp.out` to `mat_mat.out`.