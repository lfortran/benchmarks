#!/bin/bash
# Benchmarking utility script

kind=8

upperbound=16384

if [ $kind -eq 8 ]
then
    upperbound=8192;
fi

for ((i = 1024; i <= upperbound; i = i * 2)); do
    echo $1 $4 $i $i $kind
    python mat_vec.py $i $i $kind > test.f90
    $1 $4 test.f90
    hyperfine --warmup $3 --min-runs=$2 ./a.out >> mat_vec.out
done

if [ $kind -eq 4 ]
then
    echo $1 $4 25600 12800
    python mat_vec.py 25600 12800 $kind > test.f90
    $1 $4 test.f90
    hyperfine --warmup $3 --min-runs=$2 ./a.out >> mat_vec.out
fi

echo $'\n'