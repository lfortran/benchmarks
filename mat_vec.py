import sys

rows, cols = int(sys.argv[1]), int(sys.argv[2])
kind = int(sys.argv[3])

lines = ["program mat_vec",
         "implicit none",
         "integer(4) :: i, j, rows, cols",
         "real({}), allocatable :: mat(:,:), vec(:), res(:)".format(kind),
         "rows = {}".format(rows),
         "cols = {}".format(cols),
         "allocate(mat(rows, cols), vec(cols), res(rows))",
         "do i = 1, rows",
         "   do j = 1, cols",
         "       mat(i, j) = (i + j) * 0.314",
         "   end do",
         "end do",
         "do j = 1, cols",
         "   vec(j) = j * 0.172",
         "end do",
         "do i = 1, rows",
         "    res(i) = 0",
         "    do j = 1, cols",
         "        res(i) = res(i) + mat(i, j) * vec(j)",
         "    end do",
         "end do",
         "end program"]

print('\n'.join(lines))