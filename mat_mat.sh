#!/bin/bash
# Benchmarking utility script

for ((i = 128; i <= 1024; i = i * 2)); do
    echo $1 $4 $i $i
    python mat_mat.py $i $i > test.f90
    $1 $4 test.f90
    hyperfine --warmup $3 --min-runs=$2 ./a.out >> mat_mat.out
done

echo $1 $4 1280 1280
python mat_mat.py 1280 1280 > test.f90
$1 $4 test.f90
hyperfine --warmup $3 --min-runs=$2 ./a.out >> mat_mat.out

echo $'\n'